Before class: Make servers and update hosts.raw
## Exercise: Introductions and team formation

# What is Ansible?

Compare shell, puppet, ansible

## Discuss: idempotent

Idempotent English:

Is the barn red? If yes, n-yay. If no, paint the barn red.
Is the barn red? If yes, p-yay. If no, ask for help.


## Exercise: Install ansible

me: Use the blank Fedora box.

Varies, but some suggestions...

Debian - Ubuntu - Mint - Etc...

```bash
sudo apt-get update
sudo apt-get install ansible
```

Fedora

```bash
sudo dnf install ansible
```

RHEL - CentOS

```bash
sudo yum install epel-release
sudo yum install ansible
```

OpenSUSE

```bash
sudo zypper install ansible
```

## Exercise: Setup identifiers for class

me: Use the blank Fedora box.

Clone this repository

```bash
cd ~/git/ # or cd where-you-keep-your-code
git clone git@gitlab.com:sofreeus/aghi2ansible.git
# or
git clone https://gitlab.com/sofreeus/aghi2ansible.git
```

Update host keys for the machines

```bash
ansible all -i hosts.raw --list |
./update-host-key.sh
```

Decrypt vaulted pem-file

```bash
cp ansible.pem.vault ansible.pem
ansible-vault decrypt ansible.pem
# password: "SoF*****-**17!"
```

Verify ssh connectivity

```bash
ssh -i ansible.pem centos@52.41.28.89
```

Review inventory, assign machines, rename your group to 'raw'

Ansible ping the machines

```bash
export ANSIBLE_INVENTORY=hosts.raw
ansible all -m ping
ansible raw -m ping
```

## Discuss: inventory, ad-hoc, modules

## Exercise: Some ad-hoc commands (raw, yum, fetch)

Check whether I'm a user on a system.

```bash
ansible raw -a 'getent passwd root'
ansible raw -a 'getent passwd dlwillson'
ansible raw -a 'tail /etc/passwd'
```

Install a package on a server.

```bash
ansible 52.41.28.89 -m 'yum name=httpd' -b
```

Check whether a package is installed.

```bash
ansible all -a 'rpm -q chrony'
ansible all -a 'rpm -q httpd'
```

Check recent syslog messages.

```bash
ansible alicorn -a 'tail /var/log/messages' -b
```

Fetch all messages files.

```bash
ansible alicorn -m \
  'fetch src=/var/log/messages dest=/tmp/' -b
```

## Discuss: playbooks and roles

## Exercise: Add admins

1. Edit hosts.raw: Delete all machines except your team's.
*  Edit add-admins.yml to correct name/keyu values.
*  Run  ./add-admins.sh

## Discuss: trifecta (install, configure, start)

## Exercise: Make a web-server

1. Copy hosts.raw to hosts.myteam (see hosts.alicorn)
*  Edit hosts.myteam:
  - Remove everything after the IP address.
  - Add groups.
*  Edit ansiblerc and source it `. ansiblerc`
*  or `export ANSIBLE_INVENTORY=hosts.myteam`
*  Edit `webserver-playbook.yml`
*  Run `ansible-playbook webserver-playbook.yml`

## Discuss: round-robin DNS, HA/HP, SSL termination

## Exercise: Make an SSL-terminating proxy / load-balancer

* need internal IP's of app-servers
* need to loop through the app-servers in the template

## Discuss: auto-scaling

## Exercise: Make more web-servers. Add them to the load-balancer automatically. And, chaos-monkey them.

* need to gather internal ip addresses *automatically*.
