#!/bin/bash -eu
# David L. Willson
# Virtualbox
machines="haproxy proxy app1 app2 db"
basevm="Ubuntu Server 16.04"
basesnapshot="Base-SmallMem-NN"

read -p "This will delete any machines named: $machines. Do you want to continue? (y/N) " answer

if [[ $answer != "y" ]]
then
  echo "You didn't answer 'y', so I quit."
  exit
fi

for m in $machines
do
  set +e
  vboxmanage controlvm haproxy poweroff
  vboxmanage unregistervm $m --delete
  set -e
  vboxmanage clonevm Fedora --snapshot base --options link --name $m --register
done
