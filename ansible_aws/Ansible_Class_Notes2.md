# ansible class


***before class, install vbox***
* download virtual box and update (file >> check for updates)
  * https://www.virtualbox.org/wiki/Downloads

## *part 1*
***install***

> *lecture 1 - what is ansible, idempotency*

> automated configuration management / resource deployment - create resources, configure resources, enforce configuration of resources, within a framework **safer than shell scripts**, most of these products use **free-mium** cost model
  * **Ansible**
    * shorter learning curve because **yaml files**
    * push model using ssh primarily so **NO AGENTS REQUIRED**
    * Python based but little to no knowledge of python scripting required
    * Lower performance at scale  
  * **Chef**
    * longer learning curve, **must know ruby** as well as git, large code base
    * master/client model (pull model, **agents**)
    * no push model but **knife tool** simplifies deployment of agents
    * popular w/ developers
  * **puppet**
    * considered most **mature and trusted** cm tool
    *  ruby based, large complex code base
    * master/client model (pull model, **agents**)



> **why are these gaining popularity over shell/python/ruby/java scripts for SA operations?**

> idempotency in english - Is the barn red? If yes, just say so and do nothing. If no, paint the barn red.

> (safer, more efficient use of resources at scale than shell scripts)

---
*lab 1*
  * install
    * import a linux vm to virtualbox, OVAs available on mem sticks. Once OVA is installed and turned on, run `sudo apt-get update` or `sudo yum update` depending on which Linux flavor you are using.
    * install git
      * `sudo apt-get install git` or `sudo yum...`
    * download aghi2ansible from gitlab.com/sofreeus
      * make a git folder in your home(~) directory - `mkdir git`, `cd git`, `git init`
    * git clone aghi2ansible from gitlab.com/sofreeus
      * `git clone https://gitlab.com/sofreeus/aghi2ansible.git`
    * go into the newly cloned directory, unzip secrets folder, copy pem file up one level
      * `cd aghi2ansible/ansible_aws/`
      * `unzip ansible_class_secrets.zip` (enter password when prompted)
      * `cd ansible_class_secrets`
      * `cp ansible.pem ..`
      * `cd ..` go back up one directory, we will work in ~/aghi2ansible/ansible_aws from here on
    * install ansible
      * `sudo apt-get install ansible`
    * Install awscli
      * `sudo apt-get install awscli`
    * Install boto
      * `sudo apt-get install python-boto`
    * run `aws configure` and insert access key values (from access_keys.csv in aghi2ansible.git)
      * AWS Access Key ID: get from access_keys.csv
      * AWS Secret Access Key: get from access_keys.csv
      * Default region name: us-west-2

## *part 2*
***create machines and inventory***


> *lecture 2 - review the basic playbook and inventory*

>    * minimum of two parts to every play in a playbook:
      * host(s) or target(s) - the computer resource that ansible will do things to
      * tasks - the things that ansible will do to the target(s)

> https://github.com/ansible/ansible-examples

>    * inventory is the list of server names or ip addresses kept in separate file called 'hosts'
      * 'hosts' file is subdivided into groups with square brackets like this: [group example], and the servers are listed directly under each group
      * specify different groups in playbooks depending on tasks to be performed

---

*lab 2*
  * Log into aws sfs account at this url:
    * https://sofreeus.signin.aws.amazon.com/console
    * Account: sofreeus
    * User Name: itntl
    * Password: available at time of class


  * create aws ec2 machines with ansible (with tags)
    * Run `ansible-playbook aws_playbook.yml` (change instance_tags.Name: value in playbook first!)
    * Go back to AWS EC2 console, find the newly created EC2 machines with your Name tag, copy/paste the IPv4 Public IP address of each to webservers group into your hosts file (hosts file is in aghi2ansible.git)

## *part 3*
***ad hoc***

> *lecture 3 -
use ad hoc commands for things you don't need to do for every host but just need to do occasionally to lots of servers at once:*
* perform checks: ping
* troubleshoot: check storage capacity

---

*lab 3*
* update the host-key of each new server created so your ansible controller (ie: your laptop's ~/.ssh/known_hosts file) is aware of each new server
    * `ansible webservers -i hosts --list | ./update-host-key.sh`
* ping the new servers listed in your hosts file:
  * `ansible -i hosts all --private-key=ansible.pem -u ec2-user -m ping`
* perform yum -y update command on each new server listed in your hosts file:
    * `ansible -i hosts all --private-key=ansible.pem -a "df -h" -u ec2-user`

## *part 4*
***install apache on inventory, update webpage w/ html file***

> *lecture 4 - more detail about playbooks*
 * ansible module preference order
    * module that does exactly what you want is best! (ex: **package**, user, group, **service**, **ec2**, file)
    * command module is 2nd best (does only one shell command at a time for safety)
    * create pure shell/python/ruby/etc... scripts and call them w/ansible
    * shell or raw module (passes linux commands directly to shell)


---
*lab 4*
* turn your newly created servers into webservers - install, start, and enable apache. put an html file in the index directory ( /var/www/html/index.html )
  * `ansible-playbook make_webservers.yml -i hosts`
* pull up a browser, enter the IP address of one of your servers to see your new web page


---

*appendix*

> * see where ansible binaries are kept: `ls /usr/bin/ansible*`

> * see where ansible standard python modules are kept: `ls /usr/lib/python2.7/dist-packages/ansible/modules/core/`

> * see where the default ansible hosts file is kept: `ls /etc/ansible/`
