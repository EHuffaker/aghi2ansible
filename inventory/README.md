dynamic inventory
---

Just try it:

```bash
. ~/git/tds/controller/ansible/env
ansible all --list
ansible security_group_TDS_DPT2_Controllers -m ping
```

Thorough docs:

https://aws.amazon.com/blogs/apn/getting-started-with-ansible-and-dynamic-amazon-ec2-inventory-management/

http://docs.ansible.com/ansible/intro_dynamic_inventory.html#example-aws-ec2-external-inventory-script

Update the files:

```bash
wget https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.py
wget https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.ini
```
