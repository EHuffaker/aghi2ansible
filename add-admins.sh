#!/bin/bash -eu

# This script enables the users to login to the target machines
# using any ssh key they have authorized on GitLab
# and sudo without a password.
playbooks_dir="$( dirname $0)"
target="raw"
# target="all"
# target="webservers"

cd $playbooks_dir
export ANSIBLE_INVENTORY=hosts.raw
# add admins to target
ansible-playbook add-admins.yml --extra-vars="target=$target"
